// @ts-check

export default () => {
  /**
   * Check if a number is Fizz
   * @param {number} number Number to check
   */
  const ifFizz = (number) => number % 3 === 0 || number.toString().includes('3')

  /**
   * Check if number is Buzz
   * @param {number} number Number to check
   */
  const ifBuzz = (number) => number % 5 === 0 || number.toString().includes('5')

  /**
   * @type {(string | number)[]} Will contain the final array of elements
  */
  let result = []
  for (let i = 1; i <= 100; i++) {
    /**
     * @type {string | number} Will contain the final element of the array
     */
    let arrayElement = ''

    if (ifFizz(i)) arrayElement += 'Fizz'
    if (ifBuzz(i)) arrayElement += 'Buzz'
    if (arrayElement === '') arrayElement = i

    result.push(arrayElement)
  }

  return result;
}

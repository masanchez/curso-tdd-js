import fizzbuzz from "./fizzbuzz";

describe("Fizz Buzz tests", () => {
  const getRealPosition = (OrdinalPosition) => OrdinalPosition - 1

  it('returns the numbers from 1 to 100.', () => {
    expect(fizzbuzz()).toHaveLength(100)
  })

  it('For numbers which are multiples of three print "Fizz"', () => {
    expect(fizzbuzz()[getRealPosition(3)]).toEqual('Fizz')
    expect(fizzbuzz()[getRealPosition(6)]).toEqual('Fizz')
  })

  it('For numbers which are  multiples of five print "Buzz"', () => {
    expect(fizzbuzz()[getRealPosition(5)]).toEqual('Buzz')
    expect(fizzbuzz()[getRealPosition(10)]).toEqual('Buzz')
  })

  it('For numbers which are multiples of both three and five print "FizzBuzz"', () => {
    expect(fizzbuzz()[getRealPosition(15)]).toEqual('FizzBuzz')
    expect(fizzbuzz()[getRealPosition(30)]).toEqual('FizzBuzz')
  })

  it('For numbers which are not multiples of three or five print number', () => {
    expect(fizzbuzz()[getRealPosition(1)]).toEqual(1)
    expect(fizzbuzz()[getRealPosition(98)]).toEqual(98)
  })

  it('A number is Fizz if it has a 3 in it', () => {
    expect(fizzbuzz()[getRealPosition(13)]).toEqual('Fizz')
  })

  it('A number is Buzz if it has a 5 in it', () => {
    expect(fizzbuzz()[getRealPosition(52)]).toEqual('Buzz')
  })

  it('A number is FizzBuzz if it has a 3 in it and it has a 5 in it', () => {
    expect(fizzbuzz()[getRealPosition(53)]).toEqual('FizzBuzz')
  })
})

export default (password) => {
  const containNumber = RegExp(/\d/);

  if (password.length <= 8) return false
  if (password.toLowerCase() === password) return false
  if (password.toUpperCase() === password) return false
  if (!containNumber.test(password)) return false
  if (!password.includes('_')) return false

  return true
}

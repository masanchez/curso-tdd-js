import passwordValidator from "./passwordValidator"

describe("Password Validator", () => {
  it ('Is Valid Password ', () => {
    expect(passwordValidator('Abcdefg_1')).toEqual(true)
  })

  it('Have more than 8 characters', () => {
    expect(passwordValidator('Ag_1')).toEqual(false)
  })

  it ('Contains at least a capital letter', () => {
    expect(passwordValidator('abcdefg_1')).toEqual(false)
  })

  it ('Contains at least a lowercase', () => {
    expect(passwordValidator('ABCDEFG_1')).toEqual(false)
  })

  it ('Contains at least a Contains at least number', () => {
    expect(passwordValidator('ABCDEFG_s')).toEqual(false)
  })

  it ('Contains at least an underscore ', () => {
    expect(passwordValidator('ABCDEFGhs')).toEqual(false)
  })
})

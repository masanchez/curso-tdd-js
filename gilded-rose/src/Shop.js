import DefaultItem from "./DefaultItem"
import AgedBrieItem from "./AgedBrieItem";
import BackstageItem from "./BackstageItem";
import SulfurasItem from "./SulfurasItem";
export default class Shop {
  constructor(someItems) {
    this.items = someItems.map(item => {
      if(item.name === "Aged Brie") return new AgedBrieItem(item);
      if(item.name === "Backstage passes to a TAFKAL80ETC concert") return new BackstageItem(item);
      if(item.name === "Sulfuras, Hand of Ragnaros") return new SulfurasItem(item);
      return new DefaultItem(item)
    })
  }

  updateQuality() {
    this.items.forEach(item => item.updateQuality())
  }
}

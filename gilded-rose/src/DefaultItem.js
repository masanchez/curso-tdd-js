import { qualityBoundaries } from "./constants"

export default class DefaultItem {
  constructor (item) {
    this.item = item
  }

  decreaseQuality() {
    this.item.quality > qualityBoundaries.min && this.item.quality--
  }

  updateQuality() {
    this.item.sellIn--

    this.decreaseQuality()
    this.item.sellIn < 0 && this.decreaseQuality()
  }
}
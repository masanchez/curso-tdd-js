import Shop from "./Shop"
import Item from "./Item"

describe("Gilded rose", () => {

  it('All items have a SellIn value which denotes the number of days we have to sell the item', () => {
    const item = new Item("foo", 1, 3)

    expect(item.sellIn).toEqual(1)
  })

  it('All items have a Quality value which denotes how valuable the item is', () => {
    const item = new Item("foo", 1, 3)

    expect(item.quality).toEqual(3)
  })

  it('At the end of each day our system lowers both values for every item', () => {
    const item = new Item("foo", 1, 3)
    const shop = new Shop([item])

    shop.updateQuality()

    expect(item.sellIn).toEqual(0)
    expect(item.quality).toEqual(2)
  })

  it('Once the sell by date has passed, Quality degrades twice as fast', () => {
    const item = new Item("foo", 0, 3)
    const shop = new Shop([item])

    shop.updateQuality()

    expect(item.quality).toEqual(1)
  })

  it('The Quality of an item is never negative', () => {
    const item = new Item("foo", 0, 0)
    const shop = new Shop([item])

    shop.updateQuality()

    expect(item.quality).toEqual(0)
  })

  it('"Aged Brie" actually increases in Quality the older it gets', () => {

    const item = new Item("Aged Brie", 1, 3)
    const shop = new Shop([item])

    shop.updateQuality()

    expect(item.quality).toEqual(4)
  })

  it('"Aged Brie" once the sell by date has passed, Quality increases twice as fast', () => {

    const item = new Item("Aged Brie", -10, 3)
    const shop = new Shop([item])

    shop.updateQuality()

    expect(item.quality).toEqual(5)
  })

  it('The Quality of an item is never more than 50', () => {

    const item1 = new Item("Aged Brie", 1, 50)
    const item2 = new Item("Aged Brie", -10, 49)
    const shop = new Shop([item1, item2])

    shop.updateQuality()

    expect(item1.quality).toEqual(50)
    expect(item2.quality).toEqual(50)
  })

  it('"Sulfuras, Hand of Ragnaros", being a legendary item, never has to be sold or decreases in Quality', () => {

    const item = new Item("Sulfuras, Hand of Ragnaros", 1, 3)
    const shop = new Shop([item])

    shop.updateQuality()

    expect(item.sellIn).toEqual(1)
    expect(item.quality).toEqual(3)
  })

  describe("Backstage passes", () => {
    const createNewBackstagePassItem = (sellIn, quality) => new Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality)

    it('Increases in Quality as it\'s SellIn value approache', () => {

      const item = createNewBackstagePassItem(20, 3)
      const shop = new Shop([item])
  
      shop.updateQuality()
  
      expect(item.quality).toEqual(4)
    })

    it('Quality increases by 2 when there are 10 days or less', () => {

      const item = createNewBackstagePassItem(10, 3)
      const shop = new Shop([item])
  
      shop.updateQuality()
  
      expect(item.quality).toEqual(5)
    })

    it('By 3 when there are 5 days or less', () => {

      const item = createNewBackstagePassItem(5, 3)
      const shop = new Shop([item])
  
      shop.updateQuality()
  
      expect(item.quality).toEqual(6)
    })

    it('Quality drops to 0 after the concert', () => {

      const item = createNewBackstagePassItem(0, 10)
      const shop = new Shop([item])
  
      shop.updateQuality()
  
      expect(item.quality).toEqual(0)
    })
  })
})
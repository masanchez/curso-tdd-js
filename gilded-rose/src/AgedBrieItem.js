import { qualityBoundaries } from './constants'

export default class AgedBrieItem {
  constructor (item) {
    this.item = item
  }

  increaseQuality() {
    this.item.quality < qualityBoundaries.max && this.item.quality++
  }

  updateQuality() {
    this.item.sellIn--

    this.increaseQuality()
    this.item.sellIn < 0 && this.increaseQuality()
  }
}
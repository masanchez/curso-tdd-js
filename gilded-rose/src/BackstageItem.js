import { qualityBoundaries } from './constants'

export default class BackstageItem {
  constructor(item) {
    this.item = item
  }

  increaseQuality() {
    this.item.quality < qualityBoundaries.max && this.item.quality++
  }

  updateQuality() {
    this.item.sellIn--

    if (this.item.sellIn <= 0) {
      this.item.quality = 0
      return
    }

    this.increaseQuality()
    this.item.sellIn < 11 && this.increaseQuality()
    this.item.sellIn < 6 && this.increaseQuality()
  }
}
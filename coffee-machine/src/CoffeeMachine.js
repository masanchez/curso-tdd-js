export default class CoffeMachine {
  constructor(drinkMaker) {
    this.availableDrinks = {
      coffee: {
        code: "C",
        price: 0.6
      },
      tea: {
        code: "T",
        price: 0.4
      },
      chocolate: {
        code: "H",
        price: 0.5
      },
      orangeJuice: {
        code: "O",
        price: 0.6
      }
    }

    this.sugarBoundaries = {
      min: 0,
      max: 2
    }

    this.drinkMaker = drinkMaker

    this.initCoffeeMachineValues()
  }

  makeCoffee() {
    this.selectedDrink = this.availableDrinks.coffee;
  }

  makeTea() {
    this.selectedDrink = this.availableDrinks.tea;
  }

  makeChocolate() {
    this.selectedDrink = this.availableDrinks.chocolate;
  }

  makeOrangeJuice() {
    this.selectedDrink = this.availableDrinks.orangeJuice;
  }

  incrementSugar() {
    if (this.sugar < this.sugarBoundaries.max) this.sugar++
  }

  hasEnoughMoney() {
    return this.selectedDrink.price <= this.money
  }

  addMoney(amount) {
    this.money += amount
  }

  getOrderError () {
    if (!this.selectedDrink) return 'M:Drink type mandatory'
    else if (!this.hasEnoughMoney()) return `M:Add ${this.selectedDrink.price - this.money} € to buy drink`
  }

  sendToDrinkMaker() {
    const error = this.getOrderError()
    if (error) {
      this.drinkMaker.execute(error)
    } else {
      this.drinkMaker.execute(`${this.selectedDrink.code}:${this.sugar > 0 ? this.sugar : ""}:${this.sugar > 0 ? 0 : ""}`);
      this.initCoffeeMachineValues()
    }
  }

  initCoffeeMachineValues () {
    this.selectedDrink = null
    this.sugar = 0
    this.money = 0
  }
}
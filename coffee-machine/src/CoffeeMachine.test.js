import CoffeMachine from "./CoffeeMachine"

describe('Coffe Machine', () => {
  
  describe('The drink maker should receive the correct instructions if has enough money for:', () => {
    let coffeeMachine;
    let drinkMaker;
    beforeEach(() => {
      drinkMaker = {
        execute: jest.fn()
      }
    
      coffeeMachine = new CoffeMachine(drinkMaker)    
    })
    it('a coffee order', () => {
      coffeeMachine.makeCoffee()
      coffeeMachine.addMoney(0.6)
      coffeeMachine.sendToDrinkMaker()
  
      expect(drinkMaker.execute).toHaveBeenCalledWith("C::");
    })
  
    it('a tea order', () => {
      coffeeMachine.makeTea()
      coffeeMachine.addMoney(0.4)
      coffeeMachine.sendToDrinkMaker()
  
      expect(drinkMaker.execute).toHaveBeenCalledWith("T::");
    })
  
    it('a hot chocolate order', () => {
      coffeeMachine.makeChocolate()
      coffeeMachine.addMoney(0.5)
      coffeeMachine.sendToDrinkMaker()
  
      expect(drinkMaker.execute).toHaveBeenCalledWith("H::");
    })

    it('a orange juice order', () => {
      coffeeMachine.makeOrangeJuice()
      coffeeMachine.addMoney(0.6)
      coffeeMachine.sendToDrinkMaker()
  
      expect(drinkMaker.execute).toHaveBeenCalledWith("O::");
    })
  })

  describe('Drink with sugar', () => {
    let coffeeMachine;
    let drinkMaker;
  
    beforeEach(() => {
      drinkMaker = {
        execute: jest.fn()
      }
      coffeeMachine = new CoffeMachine(drinkMaker)    
    })

    it('I want to be able to send instructions to the drink maker to add one sugar', () => {
      coffeeMachine.makeCoffee()
      coffeeMachine.addMoney(0.6)
      coffeeMachine.incrementSugar()
      coffeeMachine.sendToDrinkMaker()
      
      expect(drinkMaker.execute).toHaveBeenCalledWith("C:1:0");
    })

    it('I want to be able to send instructions to the drink maker to add one sugar', () => {
      coffeeMachine.makeCoffee()
      coffeeMachine.addMoney(0.6)
      coffeeMachine.incrementSugar()
      coffeeMachine.incrementSugar()
      coffeeMachine.sendToDrinkMaker()

      expect(drinkMaker.execute).toHaveBeenCalledWith("C:2:0");
    })
  })

  describe('Error control', () => {
    let coffeeMachine;
    let drinkMaker;
  
    beforeEach(() => {
      drinkMaker = {
        execute: jest.fn()
      }
      coffeeMachine = new CoffeMachine(drinkMaker)    
    })

    it('No drink type selected', () => {
      coffeeMachine.sendToDrinkMaker()
      expect(drinkMaker.execute).toHaveBeenCalledWith("M:Drink type mandatory")
    })

    it('Not enough money', () => {
      coffeeMachine.makeCoffee()
      coffeeMachine.addMoney(0.1)
      coffeeMachine.sendToDrinkMaker()
      expect(drinkMaker.execute).toHaveBeenCalledWith("M:Add 0.5 € to buy drink")
    })
  })

})
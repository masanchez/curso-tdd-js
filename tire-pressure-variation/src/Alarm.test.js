import Alarm from "./Alarm"

describe("Tire pressure variation", () => {
  let alarmLogger = null
  let sensor = null

  beforeEach(() => {
    alarmLogger = {
      alarmIsActivated: jest.fn(),
      alarmIsDeactivated: jest.fn()
    }

    sensor = {
      popNextPressurePsiValue: jest.fn()
    }
  })

  afterEach(() => {
    alarmLogger = null,
    sensor = null
  })

  
  it('Alarm is activated in psi preassure outside range', () => {
    sensor.popNextPressurePsiValue.mockReturnValueOnce(14)
    const alarm = new Alarm(alarmLogger, sensor)

    alarm.check()

    expect(alarmLogger.alarmIsActivated).toHaveBeenCalled()
  })

  it('Alarm is deactivated when psi preassure back to normal', () => {
    sensor.popNextPressurePsiValue
      .mockReturnValueOnce(22)
      .mockReturnValueOnce(19)
    const alarm = new Alarm(alarmLogger, sensor)

    alarm.check()
    alarm.check()

    expect(alarmLogger.alarmIsDeactivated).toHaveBeenCalled()
  })
})

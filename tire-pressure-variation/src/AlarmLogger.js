export default class AlarmLogger {
  alarmIsActivated() {
    console.log('Alarm activated!')
  }

  alarmIsDeactivated() {
    console.log("Alarm deactivated!");
  }
}
import Sensor from "./Sensor";

var alarmOn = Symbol();

export default class Alarm {
  constructor(alarmLogger, sensor) {
    this.lowPressureThreshold = 17;
    this.highPressureThreshold = 21;
    this.sensor = sensor ? sensor: new Sensor();
    this[alarmOn] = false;

    this.alarmLogger = alarmLogger ? alarmLogger : new AlarmLogger()
  }

  check() {
    const psiPressureValue = this.sensor.popNextPressurePsiValue();

    if (psiPressureValue < this.lowPressureThreshold || this.highPressureThreshold < psiPressureValue) {
      if(!this[alarmOn]) {
        this[alarmOn] = true;
        this.alarmLogger.alarmIsActivated()
      }
    } else {
      if(this[alarmOn]) {
        this[alarmOn] = false;
        this.alarmLogger.alarmIsDeactivated()
      }
    }
  }
};

